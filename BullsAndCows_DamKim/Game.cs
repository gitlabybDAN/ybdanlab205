﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BullsAndCows
{
    class Game
    {
        public static Game GameInstance;
        public bool IsRunning = true;
        public int NumOfDight;
        int[] randomNumber;

        public Game()
        {
            GameInstance = this;
            ProcessGame();
        }

        private void ProcessGame()
        {

            while (IsRunning)
            {
                if (!IsRunning)
                {
                    return;
                }
                Act();
            }
        }

        private void Act()
        {
            Console.Write("Number of dights? (3 to 5) : ");
            NumOfDight = int.Parse(Console.ReadLine());
            if (NumOfDight == 3 || NumOfDight == 4 || NumOfDight == 5)
            {
                Console.WriteLine(NumOfDight + " dights.");
                GenerateNum(NumOfDight);
            }
            else
            {
                Console.WriteLine("Try again.");
                return;
            }
        }

        private void GenerateNum(int selectedDight)
        {
            randomNumber = new int[NumOfDight];
            Random randomDight = new Random();
            Console.Write("Generated : ");
            for (int i = 0; i < randomNumber.Length; i++)
            {
                randomNumber[i] = randomDight.Next(9);
                Console.Write(randomNumber[i]);
            }
            Console.WriteLine("");

            Console.Write("Write your number. ");
            int input = int.Parse(Console.ReadLine());
            if (input / (int)Math.Pow(10.0, Convert.ToDouble(NumOfDight - 1)) <= 9)
            {
                CheckNum(input);
            }
            else
            {
                Console.WriteLine("Try again.");
                return;
            }
        }

        public void CheckNum(int input)
        {
            Console.WriteLine("input : " + input);
            int Bulls = 0;
            int Cows = 0;
            int inputDight;

            for (int i = 0; i < NumOfDight; i++)
            {
                inputDight = (input / (int)Math.Pow(10.0, Convert.ToDouble(NumOfDight - i - 1))) % 10;
                if (inputDight == randomNumber[i])
                {
                    Bulls++;
                }
                else
                {
                    for (int j = 0; j < NumOfDight; j++)
                    {
                        inputDight = (input / (int)Math.Pow(10.0, Convert.ToDouble(NumOfDight - j - 1))) % 10;
                        if (inputDight == randomNumber[i])
                        {
                            Cows++;
                        }
                    }
                }
            }
            Console.WriteLine("Bulls : " + Bulls + ". Cows : " + Cows + ".");
        }

        public void ClearDialogue()
        {
            string Output = "";
            for (int i = 0; i < 17; i++)
            {
                for (int j = 0; j < Console.WindowWidth; j++)
                {
                    Output += " ";
                }
            }
            Console.Write(Output);
        }

    }
}